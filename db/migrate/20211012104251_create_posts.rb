class CreatePosts < ActiveRecord::Migration[6.0]
  def change
    create_table :posts do |t|
      t.integer :user_id
      t.integer :sign_id
      t.text :content
      t.references :student, null: false, foreign_key: true

      t.timestamps
    end
  end
end
