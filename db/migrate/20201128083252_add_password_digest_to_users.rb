#Листинг 6.32: Миграция для добавления столбца password_digest к таблице users. db/migrate/[timestamp]_add_password_digest_to_users.rb 
class AddPasswordDigestToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :password_digest, :string
  end
end
