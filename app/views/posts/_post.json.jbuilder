json.extract! post, :id, :user_id, :sign_id, :content, :student_id, :created_at, :updated_at
json.url post_url(post, format: :json)
