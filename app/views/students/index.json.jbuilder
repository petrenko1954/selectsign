json.array!(@students) do |student|
  json.extract! student, :id, :country, :fio, :lastname, :firstname, :email, :comment, :user_id
  json.url student_url(student, format: :json)
end
