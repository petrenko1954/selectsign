class Sign < ApplicationRecord
  belongs_to :user
validates :user_id, presence: true

validates :rolename, presence: true, length: { maximum: 15 }

has_many :students, dependent: :destroy


accepts_nested_attributes_for :students, allow_destroy: true


end
