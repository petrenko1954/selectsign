#Листинг 8.12: Метод log_in. app/helpers/sessions_helper.rb
#Листинг 8.14-8.15: Вспомогательный метод logged_in?. app/helpers/sessions_helper.rb
# 8.26 8.35   8.39:Листинг 9.24: Метод current_user?. :10.26

module SessionsHelper
  # Осуществляет вход данного пользователя.
  def log_in(user)
    session[:user_id] = user.id

  end
#Sept_13#session[:student_id] = student.id

#def 




 # Запоминает пользователя в постоянную сессию.
  def remember(user)
    user.remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end
#8.14, 10.26 Возвращает true, если заданный пользователь является текущим.
  def current_user?(user)
#@current_user ||= User.find_by(id: session[:user_id])
    user == current_user
  end

#8.14 Возвращает текущего пользователя, осуществившего вход (если он есть).
#Листинг 10.26: Использование обобщенного метода authenticated? в current_user.

  def current_user
    if (user_id = session[:user_id])
      @current_user ||= User.find_by(id: user_id)
    elsif (user_id = cookies.signed[:user_id])
      user = User.find_by(id: user_id)
      #if user && user.authenticated?(:remember, cookies[:remember_token])
        log_in user
        @current_user = user
      #end
    end
  end

 # Возвращает пользователя, соответствующего remember-токену в куки.
  def current_user
    if (user_id = session[:user_id])
      @current_user ||= User.find_by(id: user_id)
    elsif (user_id = cookies.signed[:user_id])
      user = User.find_by(id: user_id)
      if user && user.authenticated?(cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
  end

  # Возвращает текущего вошедшего пользователя (если есть).
def current_user        #Листинг 8.14:
    @current_user ||= User.find_by(id: session[:user_id])
     if (user_id = session[:user_id])
         @current_user ||= User.find_by(id: user_id)
       elsif (user_id = cookies.signed[:user_id])
         user = User.find_by(id: user_id)
         #if user && user.authenticated?(:remember, cookies[:remember_token])
           log_in user
           @current_user = user
       #end
end
end
  #Листинг 8.15: Возвращает true, если пользователь вошел, иначе false.
def logged_in?
    !current_user.nil?
end

def log_out
    session.delete(:user_id)
    @current_user = nil
end
 # Забывает постоянную сессии.
  def forget(user)
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end
 # Осуществляет выход текущего пользователя.
  def log_out
    forget(current_user)
    session.delete(:user_id)
    @current_user = nil
  end
#Перенаправляет Листинг 9.27: Код реализации дружелюбной переадресации. app/helpers/sessions_helper.rb 
# Перенаправляет к сохраненному расположению (или по умолчанию).
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  # Сохраняет запрошенный URL.
  def store_location
    session[:forwarding_url] = request.url if request.get?
  end
# Подтверждает вход пользователя
    def logged_in_user
           unless logged_in?
             store_location
             flash[:danger] = "Please log in."
             redirect_to login_url
           end
      end
end

